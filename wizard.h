#ifndef WIZARD_H
#define WIZARD_H
#include "humanoidcreature.h"
#include <vector>
#include <string>
class wizard: public virtual HumanoidCreature // problem diamentu - unikanie niejednoznaczności w klasach potomnych poprzez dziedziczenie virtualne (bez tego wychodziło by że dwie klasy (wizard i warrior) po których dziedziczy elf mają te same funkcje (odziedziczone po klasie HumanoidCreature)
{                                             // poprzez wirtual wskazujemy faktyczna klasę bazową dla odziedziczonych funkcji
public:  // będzie dostępne z klasy potommej i z poza niej dla obiektów Wizard i dla obiektów child - Elf, funkcje nie będą dostępne w klasach Parent
    wizard()=delete;
    explicit wizard(std::string Name);
    void Teleport(double NewPositionX, double NewPositionY);
    virtual ~wizard();
    void CastSpell(std::string SpellName);
    void LearnNewSpell(std::string NewSpell);
    void RegenrateMana(int ManaRegenration);
    int BaseAttackValue();
protected: // będzie bezpośredni dostęp z klasy potomnej
    int Mana{100};



private:    //  nie będzie bezpośredniego dostępu z klasy potomnej
    std::vector<std::string> BookOfSpells{};

};

#endif // WIZARD_H
