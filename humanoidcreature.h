#ifndef HUMANOIDCREATURE_H
#define HUMANOIDCREATURE_H
#include <string>

class HumanoidCreature
{
public:   // bezpośredni dostęp z klas potomnych i z zewnątrz
    HumanoidCreature()=delete;
    explicit HumanoidCreature(std::string ItsName);
    HumanoidCreature(const HumanoidCreature &)=delete ;
    HumanoidCreature(HumanoidCreature &&)=delete ;
    HumanoidCreature & operator=(const HumanoidCreature &) = delete ;
    HumanoidCreature & operator=(HumanoidCreature &&) = delete ;
    virtual ~HumanoidCreature();

    virtual void SayName() const;
    void ReportPosition() const;
    void ReduceHealth(int Demage);
    double PositionX;
    double PositionY;
protected: // dostępne w klasach potomnych ( i wewnątrz tej klasy)
    void RestoreHealth(int HealthToRestore); // będzie można używać w klasach pochodnych, lecz nie na zewnątrz
    std::string Name;  // będzie można używać w klasach pochodnych, lecz nie na zewnątrz


private:  // dostępne tylko wewnątrz tej klasy
    void setHealth(int NewHealth); // nie można będzie używać w klasach potomnych (ani nigdzie poza tą klasą)
    int Health;



};

#endif // HUMANOIDCREATURE_H
