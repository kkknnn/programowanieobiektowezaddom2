#include <iostream>
#include "humanoidcreature.h"
#include "wizard.h"
#include "elf.h"


// komentarze dotyczą dziedziczenia public


using namespace std;

int main()
{
    elf Hero("Legolas",5);
    wizard Enemy("Voldemort");
    HumanoidCreature NoNameGuy("NoName");
   //NoNameGuy.RestoreHealth(14) // NOK bo to proceted w tej klasie
    NoNameGuy.SayName(); // OK bo to public
    //NoNameGuy.setHealth(22); NOK bo to private w tej klasie


    Enemy.SayName();   // użycie funkcji z klasy bazowej (Parent)           // OK bo to public w klasie bazowej
    Hero.SayName();    // użycie funkcji przykrywającej tę z klasy Parent   // OK bo to public w tejże klasie

    ElvenItem Apple={"Apple",14,true};
    ElvenItem Trash={"Broken Sword",20,false};


    //Enemy.RestoreHealth(50);                      // NOK - bo to proceted w klasie bazowej
    // Enemy.setHealth(45)                          // NOK - bo to private w klasie bazowej



    Hero.LearnNewSpell("Fireball");                 // OK - bo to public w klasie odziedziczonej
    Hero.LearnNewSpell("Some stupid magical stuff");

    Hero.CastSpell("Some stupid magical stuff");  // OK - bo odziedziczone publicznie
    Hero.CastSpell("Some stupid magical stuff");
    Hero.CastSpell("Some stupid magical stuff");


    Hero.TakeItem(Trash);                       // OK - bo to public w klasie bazowej
    Hero.UseItem(0);
    Enemy.CastSpell("HokusPokus");              // OK - bo to public w tejże klasie
    Enemy.CastSpell("HokusPokus");
    Enemy.PositionX=14;                        //  OK - bo to public w klasie bazowej
    // std::cout << Enemy.BookOfSpells[0]      // NOK - bo to private w tej klasie

    // Hero=Hero;                             // NOK - bo w klasie bazowej pokasowane konstruktory kopiujące, przenoszące i operatory przypisań
    // Enemy=static_cast<wizard>(Hero);       // NOK - jw

    Hero.AttactHumanoid(Enemy,elf::AttackOptions::MagicAttack); // wersja używająca int BaseAttackValue() odziedzionej po klasie wizard
    // uncomment to try again
//    Hero.AttactHumanoid(Enemy,elf::AttackOptions::MagicAttack);


//    uncomment to punch Voldemort in his face
//    Hero.AttactHumanoid(Enemy,elf::AttackOptions::PhisicalAttack); // wersja używająca int BaseAttackValue() odziedzionej po klasie warrior

//    uncomment to kill him dead ;)
//    Hero.AttactHumanoid(Enemy,elf::AttackOptions::PhisicalAttack);



    return 0;
}
