#ifndef WARRIOR_H
#define WARRIOR_H
#include "humanoidcreature.h"

class warrior: public virtual HumanoidCreature  // unikanie niejednoznaczności w klasach potomnych poprzez dziedziczenie virtualne (bez tego wychodziło by że dwie klasy mają te same funkcje odziedziczone po klasie HumanoidCreature)
{
public:   // będzie bezporedni dostęp z klas potomnych i z poza nich
    warrior();
    int BaseAttackValue(); // można użyć na zewnątrz

private: // bez dostępu w klasach potomnych i poza nimi
    int AttackPower;


};

#endif // WARRIOR_H
