cmake_minimum_required(VERSION 2.8)

project(obiektowka-domowo2)


file (GLOB SOURCES "./*.h"
                   "./*.cpp"
                         )

add_executable(${PROJECT_NAME} ${SOURCES})
