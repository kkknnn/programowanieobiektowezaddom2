#ifndef ELF_H
#define ELF_H
#include "wizard.h"
#include "warrior.h"
#include <array>
#include <memory>
using namespace std;

struct ElvenItem
{
    std::string Name{};
    int weight{};
    bool CanBeEaten{};
};


class elf: public wizard, public warrior // klasa dziedziczy po klasach z których obie pochodzą z klasy HumanoidCreature. Zarówno wizard, jak też
{

public: // dostęp z zewnątrz (obiekty klas Warrior/Wizzard nie bedą mieć tych funkcji)
    elf()=delete ;
    elf(string Name, int Power);
    virtual ~elf();
    void TakeItem(const ElvenItem & );
    void SayName() const override;
    ElvenItem & UseItem(std::size_t ItemNumber);
    void AttactHumanoid(HumanoidCreature & AttackedHumanoid, int AttactOption = 1);
    enum AttackOptions
    {
     MagicAttack = 1,
     PhisicalAttack = 2
    };
protected:
    int Strenght; //dostęp z tej klasy, żadna inna nie dziedziczy po elfie
private:
    array<unique_ptr<ElvenItem>,15> Inventory {}; // dostęp tylko wewnątrz tej klasy

};

#endif // ELF_H
