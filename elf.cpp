#include "elf.h"
#include <iostream>
#include <array>

elf::elf(std::string Name, int Power): HumanoidCreature(Name), wizard (Name)
{
    Strenght=Power;
    std::cout << "[Grandchild - elf constructor]" << "Object " << Name << " - Look at those pointed ears!" << '\n';

}

elf::~elf()
{
    std::cout << "[Grandchild - elf destructor]" << "Object " << Name << " - The Elves are mourning" <<'\n';


}

void elf::TakeItem(const ElvenItem & ItemToTake)
{

    for(auto i=Inventory.begin();i!=Inventory.end();i++)
    {
        if (*i==nullptr)
        {

            auto toAdd = std::make_unique<ElvenItem>(ItemToTake);
            i->swap(toAdd);
            return;
        }
    }
}

void elf::SayName() const // nadpisana funcja z klasy Parent
{
    std:: cout << "Bore da, fy enw i yw " << Name << " Mae'n bleser dod i'ch adnabod" << '\n';
}

ElvenItem &elf::UseItem(std::size_t ItemNumber)
{

    auto ItemToReturn = Inventory[ItemNumber].get();
    if (ItemToReturn->CanBeEaten)
    {
        wizard::RestoreHealth(ItemToReturn->weight);
        ItemToReturn->weight=0;
    }
    return *ItemToReturn;
}

void elf::AttactHumanoid(HumanoidCreature &AttackedHumanoid, int AttactOption)
{
    if(AttactOption == 1)
    AttackedHumanoid.ReduceHealth(wizard::BaseAttackValue()*Strenght);   // by uniknąć niejednoznaczności należy zastosować modyfikatory dostępu
    else if(AttactOption==2) {
    AttackedHumanoid.ReduceHealth(warrior::BaseAttackValue()*Strenght);  // obydwie klasy mają zdefiniowane swoje BaseAttack
    }


}
