#include "humanoidcreature.h"
#include <iostream>

HumanoidCreature::HumanoidCreature(std::string ItsName): PositionX{0}, PositionY{0}, Name{ItsName}, Health{100}
{

    std::cout << "[Parent - HumanoidCreature constructor]" << "Object " << Name << " - Same Humanoid Creature has been born!" << '\n';
}

HumanoidCreature::~HumanoidCreature()
{
 std::cout << "[Parent - HumanoidCreature destructor]" << "Object " << Name << " - We won't forget him" <<'\n';
}



void HumanoidCreature::SayName() const
{

std::cout << "My name is " << Name << '\n';

}

void HumanoidCreature::ReportPosition() const
{
    std::cout << "My current coordinates are x:" << PositionX << " y:" << PositionY << '\n';

}

void HumanoidCreature::ReduceHealth(int Damage)
{
    if (Health-Damage<=0)
    {
        std::cout << "The story of " << Name << " has just ended.";
        Health=0;

    }
    else
    {
    Health-=Damage;
    }

}

void HumanoidCreature::RestoreHealth(int HealthToRestore)
{
    if(Health+HealthToRestore>=100)
    {

    setHealth(100);

    }
    else {
        setHealth(Health+=HealthToRestore);

    }
}

void HumanoidCreature::setHealth(int NewHealth)
{
    Health=NewHealth;
}
